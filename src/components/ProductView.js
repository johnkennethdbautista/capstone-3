import { useState, useEffect, useContext } from 'react'
import { Container, Card, Button, Row, Col } from 'react-bootstrap'
import { useParams, useNavigate, Link } from 'react-router-dom'
import Swal from 'sweetalert2' 
import UserContext from '../UserContext'

export default function ProductView(){
	const navigate = useNavigate()
	const {user} = useContext(UserContext)
	const [name, setName] = useState("")
	const [description, setDescription] = useState("")
	const [price, setPrice] = useState(0)
	const [quantity, setQuantity] = useState(1)
	const {productId} = useParams()

	function decrease(){
		setQuantity(quantity - 1)
	}

	function increase(){
		setQuantity(quantity + 1)
	}

	const addProduct = (productId) => {
		fetch(`${process.env.REACT_APP_API_URL}/orders/add-product`, {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				productId: productId,
				quantity: quantity
			})
		})
		.then(response => response.json())
		.then(result => {
			if(typeof result.message !== "undefined"){
				Swal.fire({
					title: "Success",
					icon: "success",
					text: result.message
				})

				// Redirect to the Courses page after enrolling
				navigate("/products")
			}
		}).catch(error => {
			Swal.fire({
					title: "Request Failed!",
					icon: "error",
					text: "An error occured while making the request"
				})
		})
	}

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/single`)
		.then(response => response.json())
		.then(result => {
			setName(result.name)
			setDescription(result.description)
			setPrice(result.price)
		})
	}, [productId])
	

	return(
		<Container className="mt-5" data-aos="zoom-in">
			<Row>
				<Col lg={{span: 6, offset: 3}}>
					<Card className="my-3"> 
						<Card.Body>
							<button className="btn btn-dark p-1 mb-3" onClick={() => navigate(-1)}>{`<`} Back</button>
							
							<Card.Title>{name}</Card.Title>
							<br/>
							<Card.Subtitle>Description:</Card.Subtitle>
							<Card.Text>{description}</Card.Text>

							<Card.Subtitle>Price:</Card.Subtitle>
							<Card.Text>Php {price.toLocaleString(undefined, {minimumFractionDigits: 2})}</Card.Text>
							{ user.id !== null ?
								<>
								<br/>
								<Card.Subtitle>Quantity:</Card.Subtitle>
								<div className= "d-flex align-items-center">
								<Button variant = "secondary" className = "btnIncrementer p-0" onClick={decrease} disabled={quantity === 1}>-</Button>
								<Card.Text className="mx-3 quantityFont">{quantity}</Card.Text>
								<Button variant = "secondary" className = "btnIncrementer p-0" onClick={increase}>+</Button>
								</div>
								<div>
								<Button className = "mt-3" variant="secondary" onClick={() => addProduct(productId)}>Order Now</Button>
								</div>
								</>
								:
								<Link className="btn btn-danger" to="/login">Log In to Order</Link>
							}
						</Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>

	)
}