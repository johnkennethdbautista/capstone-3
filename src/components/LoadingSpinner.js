import {Spinner} from 'react-bootstrap'

export default function LoadingSpinner(){
	return(
		<Spinner className="mx-auto d-block m-4 p-4" animation="border"/>
		)
}
