// To use react bootstrap components, you must first import them from the react-bootstrap package
import { Fragment, useState, useContext } from 'react'
import {Container, Navbar, Nav, NavDropdown} from 'react-bootstrap'
import { Link, NavLink } from 'react-router-dom'
import UserContext from '../UserContext'

export default function AppNavbar(){
	// Gets the user email after the user has logged in
	const { user } = useContext(UserContext)

	return(
		<Navbar className = "navbar-dark" bg="dark" expand="lg">
 			 <Container fluid className="px-5">
 			   <Navbar.Brand as={Link} to="/">Gadgets Trading</Navbar.Brand>
 			   <Navbar.Toggle aria-controls="basic-navbar-nav"/>
 			   <Navbar.Collapse id="basic-navbar-nav" className="toggle-color justify-content-end">
 			     <Nav className="ml-auto">
 			       <Nav.Link as={NavLink} to="/" className="mx-2">Home</			Nav.Link>
 			       <Nav.Link as={NavLink} to="/products" className="mx-2">			Products</Nav.Link>

 			       {user.id !== null ? (
 			         // If the user has logged in, only show the logout link
 			         <>
 			         	{user.isAdmin === true?(
 			         	<>
  <Nav.Link as={NavLink} to="/admin-dashboard" className="mx-2">Admin Dashboard</Nav.Link>
</>
 			         		) : (
 			         		<>
 			         			<NavDropdown title="Orders" id="basic-nav-dropdown">
       						   <NavDropdown.Item as={NavLink} to="/cart" 	className="	mx-2">Order</NavDropdown.Item>
       						   <NavDropdown.Item as={NavLink} to="/order-history"	 	className="mx-2">Order History</NavDropdown	.Item>
       						 </NavDropdown>
 			         		</>
 			       	 	 
 			       	 	)}
 			         	 <Nav.Link as={NavLink} to="/logout" className="mx-2">Logout</Nav.Link>
 			         </>
 			       ) : (
 			         // If the user has not logged in, show both login and 			register links
 			         <>
 			           <Nav.Link as={NavLink} to="/login" className="mx-2">Login</Nav.Link>
 			           <Nav.Link as={NavLink} to="/register" className="mx-2">Register</Nav.Link>
 			         </>
 			       )}
 			     </Nav>
 			   </Navbar.Collapse>
 			 </Container>
		</Navbar>
	)
}