import { useState, useEffect } from 'react';
import {Card, Button, Col} from 'react-bootstrap';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import AOS from 'aos'
import 'aos/dist/aos.css'

export default function ProductCard({course}){
	const {_id, name, description, price} = course 
	useEffect(() => {
    		AOS.init();
    		AOS.refresh();
  		}, []);
	return(
    <Col  sm = {12} md = {6} lg = {4} className = "my-3">
      	<Card className="mx-2 h-100" data-aos='fade-right'>
        <Card.Body className="d-flex flex-column justify-content-between">
          <Card.Title>{name}</Card.Title>
          <br/>
          <Card.Subtitle>Description:</Card.Subtitle>
          <Card.Text>{description}</Card.Text>
          <Card.Subtitle>Price:</Card.Subtitle>
          <Card.Text>
            Php {price.toLocaleString(undefined, { minimumFractionDigits: 2 })}
          </Card.Text>
          <Card.Footer className="text-center bottom">
          <Link className="btn btn-secondary" to={`/products/${_id}/view`}>
            View Product
          </Link>
          </Card.Footer>
        </Card.Body>
      </Card>
    </Col>
	)
}

// You can use prop types to validate the data from props before rendering the JSX
ProductCard.propTypes = {
	products: PropTypes.shape({ //shape() function dictates the shape/structure of the props
		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired
	})
}