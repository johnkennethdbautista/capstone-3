import { useEffect, useState, useContext } from 'react';
import UserContext from '../UserContext';
import { Link } from 'react-router-dom';

export default function OrderHistory() {
  const [paid, setPaid] = useState([]);
  const [totalPaid, setTotalPaid] = useState(0);
  const token = localStorage.getItem('token');
  const { user } = useContext(UserContext);
  const [productNames, setProductNames] = useState({})

  useEffect(() => {
    fetchPaidData();
  }, []);


  const fetchPaidData = async () => {
    try {
      const response = await fetch(`${process.env.REACT_APP_API_URL}/orders/my-order-paid`, {
        method: 'GET',
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });
      const paidData = await response.json();
      const paidProducts = paidData.data;
      let totalPaid = 0;
      for (let i = 0; i < paidProducts.length; i++) {
        totalPaid += paidProducts[i].totalAmount;
      }
      setTotalPaid(totalPaid);
      setPaid(paidProducts);
      fetchProductNames(paidProducts); // Fetch product names after fetching data
    } catch (error) {
      console.log(error);
    }
  };

  const fetchSingleProduct = async (id) => {
    try {
      const response = await fetch(`${process.env.REACT_APP_API_URL}/products/${id}/single-name`);
      const jsonData = await response.json();
      return jsonData.name;
    } catch (error) {
      console.log(error);
      return '';
    }
  };

 const fetchProductNames = async (products) => {
  try {
    const promises = products.map((item) => fetchSingleProduct(item.productId));
    const names = await Promise.all(promises);
    const nameMap = {};

    for (let i = 0; i < products.length; i++) {
      nameMap[products[i].productId] = names[i];
    }


    await setProductNames(nameMap);
  } catch (error) {
    console.log(error);
  }
};


  return (
    <>
      {user.id !== null ? (
        <>
          <div>
            <h1 className="text-center mx-auto my-5">Order History</h1>
            <table className="text-center mx-auto my-5 table">
              <thead className="table-dark">
                <tr>
                  <th>ID</th>
                  <th>Product Name</th>
                  <th>Quantity</th>
                  <th>Total</th>
                </tr>
              </thead>
              <tbody>
                {paid.map((item) => {
                  const productName2 = productNames[item.productId] || '';
                  return (
                    <tr key={item._id}>
                      <td className="col-2">{item._id}</td>
                      <td className="col-2">{productName2}</td>
                      <td className="col-2">{item.quantity}</td>
                      <td className="col-2">
                        PHP {item.totalAmount.toLocaleString(undefined, { minimumFractionDigits: 2 })}
                      </td>
                    </tr>
                  );
                })}
                <tr>
                  <th> Total</th>
                  <td/>
                  <td/>
                  <th>
                    PHP {totalPaid.toLocaleString(undefined, { minimumFractionDigits: 2 })}
                  </th>
                </tr>
              </tbody>
            </table>
          </div>
        </>
      ) : (
        <div className="text-center mt-5">
          <h1>404 Not Found</h1>
          <h3>You don't have access to this page, you need to login first</h3>
          <Link className="btn btn-secondary" to="/login">
            Log In to Order
          </Link>
        </div>
      )}
    </>
  );
}
