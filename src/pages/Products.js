import {useState, useEffect, Fragment} from 'react'
import ProductCard from '../components/ProductCard'
import { Row, Col, Container } from 'react-bootstrap'
import LoadingSpinner from '../components/LoadingSpinner'

export default function Products(){
	const [courses, setCourses] = useState([])
	const [isLoading, setIsLoading] =useState(false)

	// Will run upon the initial rendering of the 'Courses' component since there are no values on the 2nd argument which is the array.
	useEffect(() => {
		// Sets the isLoading state to true
		setIsLoading(true)

		fetch(`${process.env.REACT_APP_API_URL}/products/active`)
		.then(response => response.json())
		.then(result => {
			// Sets the isLoading state to false
			setIsLoading(false)

			setCourses(result.map(course => {
				return (
					<ProductCard key={course._id} course={course}/>
				)
			}))
		})
	}, [])

	return(
		<>
		<h1 className = "text-center my-3 mt-5"> Products </h1>
		<Container>
			<Row>
					{/*Using the 'courses' variable which will return the components to be rendered based on the data that was looped.*/}
					{ isLoading?
						<LoadingSpinner/>
						:
						courses 
					}
			</Row>
		</Container>
		</>
	)
}