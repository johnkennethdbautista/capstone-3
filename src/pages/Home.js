import imageSrc from '../images/banner.png'
import {Container} from 'react-bootstrap'

export default function Home(){
	return(
	<Container className="p-5 mx-auto text-center bg-dark text-white" fluid>
      <h1>Welcome to my homepage</h1>
      <img src={imageSrc} alt="My Image"/>
    </Container>
	)
}