import { useEffect, useState, useContext } from 'react';
import UserContext from '../UserContext';
import { Link } from 'react-router-dom';
import Swal from 'sweetalert2'
import LoadingSpinner from '../components/LoadingSpinner'
import {Modal, Button, Form} from 'react-bootstrap'
import { useNavigate } from 'react-router-dom'

export default function MyOrders() {
  const [pendingPayment, setPendingPayment] = useState([]);
  const [totalPending, setTotalPending] = useState(0);
  const token = localStorage.getItem('token');
  const { user } = useContext(UserContext);
  const [productNames, setProductNames] = useState({})
  const [isLoading, setIsLoading] =useState(false)
  const [enterPayment, setEnterPayment] = useState(0)
  const navigate = useNavigate()

  useEffect(() => {
    fetchPendingData();
  }, []);

  const fetchPendingData = async () => {
    try {
      const response = await fetch(`${process.env.REACT_APP_API_URL}/orders/my-order-pending`, {
        method: 'GET',
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });
      const pendingData = await response.json();
      const pendingProducts = pendingData.data;
      let totalPending = 0;
      for (let i = 0; i < pendingProducts.length; i++) {
        totalPending += pendingProducts[i].totalAmount;
      }
      setTotalPending(totalPending);
      setPendingPayment(pendingProducts);
      fetchProductNames(pendingProducts); // Fetch product names after fetching data
    } catch (error) {
      console.log(error);
    }
  };

  const fetchSingleProduct = async (id) => {
    try {
      const response = await fetch(`${process.env.REACT_APP_API_URL}/products/${id}/single-name`);
      const jsonData = await response.json();
      return jsonData.name;
    } catch (error) {
      Swal.fire({
        title: "Request Failed!",
        icon: "error",
        text: "An error occurred while making the request"
      })
      return '';
    }
  };

 const fetchProductNames = async (products) => {
  try {
    const promises = products.map((item) => fetchSingleProduct(item.productId));
    const names = await Promise.all(promises);
    const nameMap = {};

    for (let i = 0; i < products.length; i++) {
      nameMap[products[i].productId] = names[i];
    }


    await setProductNames(nameMap);
  } catch (error) {
    Swal.fire({
        title: "Request Failed!",
        icon: "error",
        text: "An error occurred while making the request"
      })
  }
};
  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  function payment(event) {
    // event.preventDefault()
    setIsLoading(true);
    fetch(`${process.env.REACT_APP_API_URL}/orders/payment`, {
      method: 'POST',
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      },
      body: JSON.stringify({
        payment: enterPayment
      })
    })
    .then(response => response.json())
    .then(result => {
      setIsLoading(false);
      if (typeof result.message !== "undefined") {
        Swal.fire({
          title: result.title,
          icon: result.icon,
          text: result.message
        });
      } else {
        Swal.fire({
          title: "Authentication Failed!",
          icon: "error",
          text: result.message
        });
      }
      fetchPendingData()
    })
    .catch(error => {
      // Show an error alert
      console.log(error);
      Swal.fire({
        title: "Request Failed!",
        icon: "error",
        text: error.message
      });
    });
  }

function modal() {
  return (
    <>
    <button className="btn btn-primary p-1 mx-1" onClick = {handleShow}> Checkout </button>
    <Modal show={show} onHide={handleClose}>
      <Modal.Header closeButton>
        <Modal.Title>Payment Details</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Form onSubmit={(event) => payment(event)}>
          <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
            <Form.Label>Payment</Form.Label>
            <Form.Control
              type="number"
              placeholder="Enter your payment"
              value = {enterPayment}
              onChange={event => setEnterPayment(event.target.value)}
              autoFocus
            />
          </Form.Group>
        </Form>
      </Modal.Body>
      <Modal.Footer>
        <Button variant="secondary" onClick={handleClose}>
          Close
        </Button>
        <Button variant="primary" onClick = {() => {
                  payment()
                  handleClose()
                }}>
          Proceed
        </Button>
      </Modal.Footer>
    </Modal>
    </>
  );
}

function deleteOrder(id){
    setIsLoading(true)
    fetch(`${process.env.REACT_APP_API_URL}/orders/${id}/delete-order`, {
      method: 'DELETE',
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      }
    })
    .then(response => response.json())
    .then(result => {
      setIsLoading(false)
      Swal.fire({
        title: "Deleted",
        icon: "success",
        text: "Item has been deleted"
      })
      fetchPendingData()
    })
    .catch(error => {
      Swal.fire({
        title: "Error",
        icon: "error",
        text: "An error occured"
      })
    })
}


  return (
    <>
      {user.id !== null ? (
        <>
          <div>
            <h1 className="text-center mx-auto my-5">My Orders</h1>
            <table className="text-center mx-auto my-5 table">
              <thead className="table-dark">
                <tr>
                  <th>ID</th>
                  <th>Date of Order</th>
                  <th>Product Name</th>
                  <th>Quantity</th>
                  <th>Total</th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
                {pendingPayment.map((item) => {
                  const productName1 = productNames[item.productId] || '';
                  return (
                    <tr key={item._id}>
                      <td>{item._id}</td>
                      <td>{new Date(item.purchasedOn).toLocaleString('en-US', {year: 'numeric', month: 'long', day: 'numeric', hour: 'numeric', minute: 'numeric'})}</td>
                      <td>{productName1}</td>
                      <td>{item.quantity}</td>
                      <td>
                        PHP {item.totalAmount.toLocaleString(undefined, { minimumFractionDigits: 2 })}
                      </td>
                      <td>
                      <button className="btn btn-danger p-1 mx-1" onClick={() => deleteOrder(item._id)}> Delete </button>
                      </td>
                    </tr>
                  );
                })}
                <tr>
                  <th> Total</th>
                  <td/>
                  <td/>
                  <td/>
                  <th>
                    PHP {totalPending.toLocaleString(undefined, { minimumFractionDigits: 2 })}
                  </th>
                  <td>
                    {modal()}
                   </td>
                </tr>
              </tbody>
            </table>
          </div>
        </>
      ) : (
        <div className="text-center mt-5">
          <h1>404 Not Found</h1>
          <h3>You don't have access to this page, you need to login first</h3>
          <Link className="btn btn-secondary" to="/login">
            Log In to Order
          </Link>
        </div>
      )}
    </>
  );
}
