import { useState, useEffect, useContext } from 'react';
import { Form, Button, Row, Col } from 'react-bootstrap'
import { Navigate, useNavigate } from 'react-router-dom'
import UserContext from '../UserContext'
import Swal from 'sweetalert2'

export default function Register(){
	const { user } = useContext(UserContext)

	const navigate = useNavigate()
	const [firstName, setFirstName] = useState('')
	const [lastName, setLastName] = useState('')
	const [email, setEmail] = useState('')
	const [mobileNo, setMobileNo] = useState('')
	const [password1, setPassword1] = useState('')
	const [password2, setPassword2] = useState('')
	const [isActive, setIsActive] = useState(false)


	function registerUser(event){
		event.preventDefault()

		/*// For clearing out the form
		setEmail('')
		setPassword1('')
		setPassword2('')*/
		fetch(`${process.env.REACT_APP_API_URL}/users/check-email`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email
			})
		})
		.then(response => response.json())
		.then(result => {
			if(result !== true){
			fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				firstName: firstName,
				lastName: lastName,
				email: email,
				password: password1
			})
		})
		.then(response => response.json())
		.then(result => {
			// Checks if the response is an access token. If not, then it won't store it to the localStorage
			/*if(typeof result.accessToken !== 'undefined'){
				// Not only do we set the token in the localStorage, we also set the user details to the global 'user' state/context
				localStorage.setItem('token', result.accessToken)
				retrieveUserDetails(result.accessToken)

				// For clearing out the form
				setEmail('')
				setPassword1('')

				// Show an alert
				Swal.fire({
					title: "Login Successful pareh",
					icon: "success",
					text: "Welcome to Zuitt!"
				})
			} */
						// For clearing out the form
						setFirstName('')
						setLastName('')
						setEmail('')
						setPassword1('')
						setPassword2('')
			
						Swal.fire({
							title: "Succes!",
							icon: "success",
							text: result.message
						})
						navigate('/login')
		}).catch(error => {
			// Show an error alert
			Swal.fire({
				title: "Authentication failed",
				icon: "error",
				text: error.message
			})
			console.log(error)
		})
		} else {
			Swal.fire({
							title: "Duplicate Email Found",
							icon: "error",
							text: "Please provide a different email"
						})
		}
		})
		

		/*// Show an alert
		alert("Thank you for registering!")*/
	}

	// For form validation, we use the 'useEffect' to track the values of the input fields and run a validation condition everytime there is user input in those fields.
	useEffect(() => {
		// All input fields must not be empty and the password/verify passwords fields must match in values.
		if((email !== '' && password1 !== '' && password2 !== '') && (password1 === password2)){
			setIsActive(true)
		} else {
			setIsActive(false)
		}
	}, [email, password1, password2])

	// MINI ACTIVITY (20 mins.) 7:55PM
	// Conditionally render the register form to check first if the user is logged in. If so, then redirect to the Courses page, but if not then show the register form.
	// Once finished, push to gitlab and link to boodle.
	return(
		(user.id !== null) ?
		<Navigate to="/products"/>
		: 
		<Row>
		<Col md={6} className="mx-auto mt-5">
		<Form onSubmit={(event) => registerUser(event)}>
		<h1 className="text-center">Register</h1>
		<Form.Group controlId="firstName">
		<Form.Label>First Name</Form.Label>
		<Form.Control 
		type="text" 
		placeholder="Enter first name" 
		value={firstName}
		onChange={event => setFirstName(event.target.value)}
		required
		/>
		<Form.Label>Email address</Form.Label>
		</Form.Group>
		<Form.Group controlId="lastName">
		<Form.Label>Last Name</Form.Label>
		<Form.Control 
		type="text" 
		placeholder="Enter last name" 
		value={lastName}
		onChange={event => setLastName(event.target.value)}
		required
		/>
		<Form.Label>Email address</Form.Label>
		</Form.Group>
		<Form.Group controlId="userEmail">
	{/* 2-way Data Binding is when the value of the state reflects on the value of the input field, and vice-versa. The way to implement it is by using an 'onChange' event listener on the input field and updating the value of the state to the current value of the input field. */}
	<Form.Control 
	type="email" 
	placeholder="Enter email" 
	value={email}
	onChange={event => setEmail(event.target.value)}
	required
	/>
	<Form.Text className="text-muted">
	We'll never share your email with anyone else.
	</Form.Text>
	</Form.Group>

	<Form.Group controlId="password1">
	<Form.Label>Password</Form.Label>
	<Form.Control 
	type="password" 
	placeholder="Password"
	value={password1}
	onChange={event => setPassword1(event.target.value)} 
	required
	/>
	</Form.Group>

	<Form.Group className = "mb-2" controlId="password2">
	<Form.Label>Verify Password</Form.Label>
	<Form.Control 
	type="password" 
	placeholder="Verify Password" 
	value={password2}
	onChange={event => setPassword2(event.target.value)}
	required
	/>
	</Form.Group>

{/* Conditional Rendering is when you render elements depending on a specific logical condition */}
{ isActive ?
	<Button variant="secondary" type="submit" id="submitBtn">
	Submit
	</Button>
	:
	<Button disabled variant="secondary" type="submit" id="submitBtn">
	Submit
	</Button>
}
</Form>
</Col>
</Row>
)
}