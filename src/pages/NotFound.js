import { Link, useNavigate } from 'react-router-dom'
import { Button } from 'react-bootstrap'
import Login from './Login'

export default function NotFound(){
	// Initializing useNavigate as 'navigate' variable
	const navigate = useNavigate()

	return(
		<div className = "text-center m-5">
			<h1>Page Not Found</h1>
			<p> The page you are looking for cannot be found</p>
			<Button variant="secondary" onClick={() => navigate(-1)}>Go back</Button>
		</div>
	)
}