import { useEffect, useState, useContext } from 'react';
import UserContext from '../UserContext';
import { Link } from 'react-router-dom';
import Swal from 'sweetalert2'
import LoadingSpinner from '../components/LoadingSpinner'
import {Modal, Button, Form} from 'react-bootstrap'
import { useNavigate } from 'react-router-dom'

export default function Admin() {
  const [adminDashboardProducts, setAdminDashboardProducts] = useState([]);
  const token = localStorage.getItem('token');
  const { user } = useContext(UserContext);
  const [productNames, setProductNames] = useState({})
  const [isLoading, setIsLoading] =useState(false)
  const [name, setName] = useState('')
  const [description, setDescription] = useState('')
  const [price, setPrice] = useState('')
  const [isActive, setIsActive] = useState('')
  const navigate = useNavigate()
  const [updateProductName, setUpdateProductName] = useState('')
  const [updateProductDescription, setUpdateProductDescription] = useState('')
  const [updateProductPrice, setUpdateProductPrice] = useState('')
  const [updateProductIsActive, setUpdateProductIsActive] = useState('')
  const [productName,setProductName] = useState('')
  const [productDescription, setProductDescription] = useState('')
  const [productPrice, setProductPrice] = useState('')
  const [productIsActive, setProductIsActive] = useState('')
  const [valueProductName, setValueProductName] = useState('')
  const [productId, setProductId] = useState('')

  useEffect(() => {
    fetchAllProducts();
  }, []);

  const fetchAllProducts = async () => {
    try {
      const response = await fetch(`${process.env.REACT_APP_API_URL}/products/`);
      const productsData = await response.json();
      const allProducts = productsData;
      setAdminDashboardProducts(allProducts);
    } catch (error) {
      console.log(error);
    }
  };

  function addProduct () {
 fetch(`${process.env.REACT_APP_API_URL}/products/add-product`, {
        method: 'POST',
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json"
        },
        body: JSON.stringify({
        name: name,
        description: description,
        price: price
      })
      })
      .then(response => response.json())
      .then(result => {
      setIsLoading(false);
      if (result && result.message) {
        Swal.fire({
          title: "Success",
          icon: "success",
          text: result.message
        });
        setName('')
        setDescription('')
        setPrice('')
      } else {
        Swal.fire({
          title: "Authentication Failed!",
          icon: "error",
          text: result.message
        });
      }
      fetchAllProducts()
    })
      .catch(error=>{
        console.log(error)
      })
}

  const [showAddProduct, setShowAddProduct] = useState(false);
  const addProductModalClose = () => setShowAddProduct(false);
  const addProductModalShow = () => setShowAddProduct(true);
function addProductModal() {
  return (
    <>
    <button className="btn btn-primary" onClick = {addProductModalShow}>Add Product</button>
    <Modal show={showAddProduct} onHide={addProductModalClose}>
      <Modal.Header closeButton>
        <Modal.Title>Product Information</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Form>
          <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
            <Form.Label>Product Name</Form.Label>
            <Form.Control
              type="text"
              placeholder="Enter the product name"
              value = {name}
              onChange={event => setName(event.target.value)}
              autoFocus
            />
            <Form.Label>Description</Form.Label>
            <Form.Control
              as="textarea"
              rows={5}
              placeholder="Enter the product description"
              value = {description}
              onChange={event => setDescription(event.target.value)}
              autoFocus
            />
            <Form.Label>Price</Form.Label>
            <Form.Control
              type="number"
              placeholder="Enter the product price"
              value = {price}
              onChange={event => setPrice(event.target.value)}
              autoFocus
            />
          </Form.Group>
        </Form>
      </Modal.Body>
      <Modal.Footer>
        <Button variant="secondary" onClick={addProductModalClose}>
          Close
        </Button>
        <Button variant="primary" onClick = {() => {
                  addProduct()
                  addProductModalClose()
                }}>
          Proceed
        </Button>
      </Modal.Footer>
    </Modal>
    </>
  );
}

function activateProduct(id){
  fetch(`${process.env.REACT_APP_API_URL}/products/${id}/activate`, {
        method: 'PATCH',
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json"
        }
      })
      .then(response => response.json())
      .then(result => {
      setIsLoading(false);
      if (result && result.message) {
        Swal.fire({
          title: "Product Activated",
          icon: "success",
          text: result.message
        })
      } else {
        Swal.fire({
          title: "Authentication Failed!",
          icon: "error",
          text: result.message
        })
      }
      fetchAllProducts()
    })
      .catch(error=>{
        console.log(error)
      })
}
function archiveProduct(id){
  fetch(`${process.env.REACT_APP_API_URL}/products/${id}/archive`, {
        method: 'PATCH',
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json"
        }
      })
      .then(response => response.json())
      .then(result => {
      setIsLoading(false);
      if (result && result.message) {
        Swal.fire({
          title: "Product put to Archive",
          icon: "success",
          text: result.message
        })
      } else {
        Swal.fire({
          title: "Authentication Failed!",
          icon: "error",
          text: result.message
        })
      }
      fetchAllProducts()
    })
      .catch(error=>{
        console.log(error)
      })
}
const productView = (id) => {
  fetch(`${process.env.REACT_APP_API_URL}/products/${id}/single`,{
    method: 'GET',
    headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`,
        "Content-Type": "application/json"
    }
  })
    .then(response => response.json())
    .then(result => {
      setProductName(result.name)
      setProductDescription(result.description)
      setProductPrice(result.price)
      setProductId(result._id)
    })
  .catch(error=>{
        console.log(error)
      })
}
const viewAllOrders = () => {
  fetch(`${process.env.REACT_APP_API_URL}/orders/`,{
    method: 'GET',
    headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`,
        "Content-Type": "application/json"
    }
  })
    .then(response => response.json())
    .then(result => {
      setProductName(result.name)
      setProductDescription(result.description)
      setProductPrice(result.price)
      setProductId(result._id)
    })
  .catch(error=>{
        console.log(error)
      })
}
 function updateProduct (id) {
 fetch(`${process.env.REACT_APP_API_URL}/products/${id}/update`, {
        method: 'PATCH',
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json"
        },
        body: JSON.stringify({
          name: productName,
          description: productDescription,
          price: productPrice,
        })
      })
      .then(response => response.json())
      .then(result => {
      setIsLoading(false);
      if (result && result.message) {
        Swal.fire({
          title: "Product Details Changed",
          icon: "success",
          text: result.message
        })
      setProductName('')
      setProductDescription('')
      setProductPrice('')
      } else {
        Swal.fire({
          title: "Authentication Failed!",
          icon: "error",
          text: result.message
        });
      }
      fetchAllProducts()
    })
      .catch(error=>{
        console.log(error)
      })
}
  const [showUpdateProduct, setShowUpdateProduct] = useState(false);
  const updateProductModalClose = () => setShowUpdateProduct(false);
  const updateProductModalShow = () => setShowUpdateProduct(true);

  return (
    <>
      {user.id !== null ? (
        <>
            <h1 className="text-center mx-auto my-5">Admin Dashboard</h1>
            <div className="text-center">
            {addProductModal()}
            </div>
          <div>
            <table className="text-justify mx-auto my-5 table">
              <thead className="table-dark">
                <tr>
                  <th>Product Name</th>
                  <th>Description</th>
                  <th>isActive</th>
                  <th>Total</th>
                  <th className="px-5">Actions</th>
                </tr>
              </thead>
              <tbody>
                {adminDashboardProducts.map((item) => {
                  return (
                    <tr key={item._id}>
                      <td>{item.name}</td>
                      <td>{item.description}</td>
                      <td>{item.isActive.toString()}</td>
                      <td>
                        PHP {item.price.toLocaleString(undefined, { minimumFractionDigits: 2 })}
                      </td>
                      <td>
                      <>
                      <button className="btn btn-warning p-1 mx-1" onClick={() => {productView(item._id);
                        updateProductModalShow()}}> Edit </button>
                      <Modal show={showUpdateProduct} onHide={updateProductModalClose}>
                        <Modal.Header closeButton>
                          <Modal.Title>Product Information</Modal.Title>
                        </Modal.Header>
                        <Modal.Body>
                          <Form>
                            <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
                              <Form.Label>Product Name</Form.Label>
                              <Form.Control
                                type="text"
                                placeholder="Enter the product name"
                                value = {productName}
                                onChange={event => setProductName(event.target.value)}
                                autoFocus
                              />
                              <Form.Label>Description</Form.Label>
                              <Form.Control
                                as="textarea"
                                rows={5}
                                placeholder="Enter the product description"
                                value = {productDescription}
                                onChange={event => setProductDescription(event.target.value)}
                                autoFocus
                              />
                              <Form.Label>Price</Form.Label>
                              <Form.Control
                                type="number"
                                placeholder="Enter the product price"
                                value = {productPrice}
                                onChange={event => setProductPrice(event.target.value)}
                                autoFocus
                              />
                            </Form.Group>
                          </Form>
                        </Modal.Body>
                        <Modal.Footer>
                          <Button variant="secondary" onClick={updateProductModalClose}>
                            Close
                          </Button>
                          <Button variant="primary" onClick = {() => {
                                    updateProduct(productId)
                                    updateProductModalClose()
                                  }}>
                            Proceed
                          </Button>
                        </Modal.Footer>
                      </Modal>
                      </>
                      {!item.isActive?
                        (
                      <button className="btn btn-info" onClick={() => activateProduct(item._id)}>Activate</button>
                      ) : (
                        <button className="btn btn-danger" onClick={() => archiveProduct(item._id)}>Archive</button>
                      )
                      }
                      </td>
                    </tr>
                  );
                })}
              </tbody>
            </table>
          </div>
        </>
      ) : (
        <div className="text-center mt-5">
          <h1>404 Not Found</h1>
          <h3>You don't have access to this page, you need to login first</h3>
          <Link className="btn btn-secondary" to="/login">
            Log In to Order
          </Link>
        </div>
      )}
    </>
  )
}
