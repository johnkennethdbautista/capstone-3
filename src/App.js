import './App.css';
import {UserProvider} from './UserContext'
import {BrowserRouter as Router} from 'react-router-dom'
import {Route, Routes} from 'react-router-dom'
import {useState} from 'react'
import {Container} from 'react-bootstrap'
import AppNavbar from './components/AppNavbar'
import Products from './pages/Products'
import ProductView from './components/ProductView'
import Login from './pages/Login'
import Logout from './pages/Logout'
import Orders from './pages/Orders'
import NotFound from './pages/NotFound'
import Register from './pages/Register'
import OrderHistory from './pages/OrderHistory'
import Admin from './pages/Admin'
import Home from './pages/Home'

function App() {
  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  })

  const unsetUser = () => {
    localStorage.clear()
  }
  return (
   <UserProvider value = {{user, setUser, unsetUser}}>
        <Router>
            <AppNavbar/>
            <Container>
              <Routes>
                <Route path="/products" element={<Products/>}/>
                <Route path="/" element={<Home/>}/>
                <Route path="/products/:productId/view" element={<ProductView/>}/>
                <Route path="/cart" element={<Orders/>}/>
                <Route path="/order-history" element={<OrderHistory/>}/>
                <Route path='/login' element={<Login/>}/>
                <Route path='/logout' element={<Logout/>}/>
                <Route path='/register' element={<Register/>}/>
                <Route path='/admin-dashboard' element={<Admin/>}/>

                <Route path="*" element={<NotFound/>}/>
              </Routes>
            </Container>
        </Router>
   </UserProvider> 
  )
}

export default App;
